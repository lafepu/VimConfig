autocmd! bufwritepost .vimrc source %
autocmd ColorScheme * highlight Error  ctermbg=red guibg=red
autocmd InsertLeave * match Error /\s\+$/

set encoding=utf-8  " The encoding displayed.
set fileencoding=utf-8  " The encoding written to file.
set nobomb "Remove BOM

filetype off
filetype plugin indent on
syntax on

if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Commenting blocks of code.
autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType tex              let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
autocmd FileType lua              let b:comment_leader = '-- '
autocmd FileType yaml             let b:comment_leader = '# '

au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml "foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2

set listchars=eol:¬,tab:\|\ ,trail:·,extends:>,precedes:<

function! CommentToggle()
    execute ':silent! s/\([^ ]\)/' . b:comment_leader . ' \1/'
    execute ':silent! s/^\( *\)' . b:comment_leader . ' \?' . b:comment_leader . ' \?/\1/'
endfunction
vmap <C-_> :call CommentToggle()<CR>

function! ToggleVimExplorer()
  if exists("t:expl_buf_num")
      let expl_win_num = bufwinnr(t:expl_buf_num)
      if expl_win_num != -1
          let cur_win_nr = winnr()
          exec expl_win_num . 'wincmd w'
          close
          "exec cur_win_nr . 'wincmd w'
          unlet t:expl_buf_num
      else
          unlet t:expl_buf_num
      endif
  else
      exec '1wincmd w'
      Vexplore
      " After switching to netwr buff, lets resize to 45
      vertical resize 25
      let t:expl_buf_num = bufnr("%")
  endif
endfunction

map <silent><F4> :call ToggleVimExplorer()<CR>

let g:netrw_banner = 0
"   1 - open files in a new horizontal split
"   2 - open files in a new vertical split
"   3 - open files in a new tab
"   4 - open in previous window
let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_altv = 1
let g:netrw_winsize = 15

map <C-Left> :tabprevious<CR>
map <C-Right> :tabnext<CR>

" Press F7 to toggle highlighting on/off, and show current value.
map <F7> :set hlsearch! hlsearch?<CR>

map <F5> :set paste! paste?<CR>
imap <F5> :set paste! paste?<CR>

map <silent> <F2> :set invnumber<CR>
imap <silent> <F2> :set invnumber<CR>

map <silent> <F1> :set invlist<CR>
imap <silent> <F1> :set invlist<CR>

nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

" toggle between window
map <S-Tab> <c-w>w

" Fast window resizing with Shift an arrow keys
if bufwinnr(1)
    map <S-Up> <C-W>+
    map <S-Down> <C-W>-
    map <S-Left> <c-w><
    map <S-Right> <c-w>>
endif

fu! ToggleCurline ()
  if &cursorcolumn
    set nocursorcolumn
  else
    set cursorcolumn
  endif
endfunction

map <silent><F3> :call ToggleCurline()<CR>

set tw=79
set nowrap
set fo-=t
set undolevels=700

set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround

set hlsearch
set incsearch
set ignorecase
set smartcase

set backup
set backupdir=~/.vim/backup
set noswapfile

set t_Co=16
set background=dark
let g:solarized_termcolors=256
colorscheme default

set cursorline
"Ukryj, wszystkie 'nieaktywne' bufory, zamiast usuwac z pamieci
set hidden
""pamietaj 1000 linii historii (komendy, wyszukiwanie, itp)
set history=1000
"Ignoruj wielkosc znakow
set ignorecase
""Zawsze pokazuj pasek statusu
set laststatus=2
"Nie przerysowuj ekranu podczas wykonywania makr, rejestrow itp
set lazyredraw
"Ustaw zawartosc linii statusu
set rulerformat=%l,%c%V%=#%n\ %3p%%
""Minimalna liczba wierszy zawsze widocznych nad i pod kursorem
set scrolloff=5
"Ustawienia sprawdzania pisowni
set spelllang=pl,en

"Konfiguracja informacji zapisywanych w pliku .viminfo
set viminfo='20,<1000,h,f0
""Dodaj klawisze kursora do przechodzenia pomiedzy liniami
set whichwrap+=<,>,[,]
"Pokazuje liste mozliwych dopelnien na pasku statusow
set wildmenu
